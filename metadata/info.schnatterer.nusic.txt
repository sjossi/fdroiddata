Categories:Multimedia
License:GPLv3
Author Name:schnatterer
Web Site:
Source Code:https://github.com/schnatterer/nusic
Issue Tracker:https://github.com/schnatterer/nusic/issues
Changelog:https://github.com/schnatterer/nusic/blob/develop/CHANGELOG.md

Name:nusic
Summary:Informs about new music of artists stored on the device
Description:
nusic uses MusicBrainz - the free music encyclopedia - to find out about new
releases of the artists on your phone. No account necessary.

Please be patient on the first start of the app. It will be faster once
initialized! Note that this app is not optimized for tablets, yet. Please be
patient. This app is open source. It's brought to you for free and is
manufactured by developers in their free time.

If you should encounter any errors or like to have new features, please give us
the opportunity to react before giving a poor rating. Please report any issues
by email (see bellow or via the app) or even better create a ticket
[https://github.com/schnatterer/nusic/issues here]. Developers: You're welcome
to contribute, fork us on [https://github.com/schnatterer/nusic GitHub]. Cheers!

What kind of permission does nusic require and why does it require them? -
Network communication, full network access: Check MusicBrainz for new releases
und Download Covers from Cover Art Archive - Network communication, view network
connections: Get notified about available connection to the Internet in order to
start checking for new releases. - Your applications information, run at
startup: Schedule regular checking for new releases via the Android alarm
manager - System tools, test access to protected storage: Get the artists that
are stored on the device - Affects Battery, prevent phone from sleeping: Prevent
the device from falling back to sleep while searching for new releases
.

Repo Type:git
Repo:https://github.com/schnatterer/nusic

Build:3.0.0,18
    commit=v.3.0.0
    subdir=nusic-apk
    gradle=yes

Auto Update Mode:None
Update Check Mode:HTTP
Update Check Data:https://raw.githubusercontent.com/schnatterer/nusic/master/gradle.properties|versionCode=(.*)|.|version=(.*)
Current Version:3.0.0
Current Version Code:18
